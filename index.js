const express = require('express');
const app = express();

app.get('/*', (req, res) => {
    const data = {
        url: req.url,
        params: req.params,
        queryParams: req.query,
        headers: req.headers,
        body: req.body
    }

    console.info(req);

    res.send(data);
});

app.post('/*', (req, res) => {
    const data = {
        url: req.url,
        params: req.params,
        queryParams: req.query,
        headers: req.headers,
        body: req.body
    }

    console.info(req);

    res.send(data);
});

app.listen(51234, () => console.log('Echo server running on 51234!'));